#ifndef SWAP_H
#define SWAP_H

template <class dt>
void swap(dt& a, dt& b);

#endif
