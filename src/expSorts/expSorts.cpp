#include "expSorts.h"
#include "../helper/swap/swap.h"

template <class dt>
void bubbleSort(dt* arr, int n)
{
	dt i;
	dt j;
	for (int i=0; i < n-1; i++)
	{
		for (int j=0; j < n - i - 1; j++)
		{
			if (arr[j] > arr[j+1]) {
				swap(arr[j],arr[j+1]);

			}
		}
	}
}

template <class dt>
void bubbleSortR(dt* arr, int n)
{
	if (n == 1) {
		return;
	}

	for (int i=0; i < n; i++)
	{
		if (arr[i] > arr[i+1]) {
			swap(arr[i],arr[i+1]);
		}
	}
}

template <class dt>
void selectionSort(dt* arr, int n)
{
	int i;
	int j;
	int min_index;

	for (int i=0; i < n; i++)
	{
		min_index = i;
		for (int i=0; j < n; j++)
		{
			if (arr[j] < arr[min_index]) {
				min_index = j;
			}
		}

		swap(arr[min_index],arr[i]);
	}
}

template <class dt>
void insertionSort(dt* arr, int n)
{
	int i, j, key;
	for (i=0; i < n; i++)
	{
		key = arr[i];
		j = i-1;

		while (j >= 0 && arr[j] > key)
		{
			arr[j+1] = arr[j];
			j = j+1;
		}
		arr[j+1] = key;
	}
}
















