#ifndef EXPSORTS_H
#define EXPSORTS_H

#include "../helper/helper.h"

template <class dt>
void bubbleSort(dt* arr,int n);

template <class dt>
void bubbleSortR(dt* arr,int n);

template <class dt>
void selectionSort(dt* arr,int n);

template <class dt>
void insertionSort(dt* arr,int n);

//#include "expSorts.cpp"
#endif
