#include "swap.h"

template <class dt>
void swap(dt& a, dt& b)
{
	dt c(a);
	a = b;
	b = c;
}
